﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace DDT.XML_DDT_GetData
{
    class DataRozetka_DDT
    {
        private static IEnumerable<TestCaseData> TestData()
        {
            var doc = XDocument.Load(System.AppDomain.CurrentDomain.BaseDirectory + "../../XML_DDT_GetData/Rozetka.xml");
            var res = from vars in doc.Descendants("data")
                      let product = vars.Attribute("product").Value
                      let name = vars.Attribute("name").Value
                      let priceless = Convert.ToDouble(vars.Attribute("priceless").Value)
                      let url = vars.Attribute("url").Value
                      select new TestCaseData(product, name, priceless, url);
            foreach (TestCaseData t in res)
                yield return t;
        }
    }
}
