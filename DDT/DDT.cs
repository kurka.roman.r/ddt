﻿using DDT.Pages;
using DDT.XML_DDT_GetData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;

namespace DDT
{
    [TestFixture]
    public class DDT
    {
        [TestMethod]
        [TestCaseSource(typeof(DataRozetka_DDT), "TestData")]
        public void TestRozetka(string product, string name, double priceless, string url)
        {
            IWebDriver driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(url);

            HomePage home = new HomePage(driver);
            home.FindsByText(product);
     
            SearchPage search = new SearchPage(driver);
            search.FindsByTextBrand(name);
            search.Rang();
            search.AddToShoppingCartFirstElement(); 
            search.OpenShoppingCart();
        
            ShoppingCartPage shoppingCart = new ShoppingCartPage(driver);


            NUnit.Framework.Assert.IsTrue(search.ShoppingCartPrice() > priceless
                || shoppingCart.ShoppingCartPrice() > priceless);

            driver.Quit();
        }
    }
}
