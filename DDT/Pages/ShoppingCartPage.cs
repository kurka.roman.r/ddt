﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDT.Pages
{
    class ShoppingCartPage: BasePage
    {
        public ShoppingCartPage(IWebDriver driver) : base(driver) { }

        [FindsBy(How = How.XPath, Using = "//div[@class='cart-receipt__sum-price']/span")]
        private IWebElement ShoppingCartWindow;

        public double ShoppingCartPrice()
        {
            return Convert.ToDouble(ShoppingCartWindow.Text);
        }
    }
}
