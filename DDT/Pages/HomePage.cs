﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDT.Pages
{
    class HomePage: BasePage
    {
        public HomePage(IWebDriver driver) : base(driver) { }

        [FindsBy(How = How.XPath, Using = "//input[@name='search']")]
        private IWebElement SearchInput;
        [FindsBy(How = How.XPath, Using = "//button[contains(text(),'Знайти')]")]
        private IWebElement SearchButton;

        public void FindsByText(string text)
        {
            SearchInput.SendKeys(text);
            SearchButton.Click();
        }
    }
}
